//
//  main.m
//  POI
//
//  Created by Zach Stegall on 8/1/14.
//  Copyright (c) 2014 Cover Media Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CMLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CMLAppDelegate class]));
    }
}
