//
//  CMLViewController.m
//  POI
//
//  Created by Zach Stegall on 8/2/14.
//  Copyright (c) 2014 Cover Media Labs. All rights reserved.
//

#import "CMLViewController.h"
#import "CMLPOIEngine.h"
#import "CMLPointView.h"
#import "CMLVector.h"
#import "UIView+Transform.h"
#import <Mapbox/Mapbox.h>

#import <QuartzCore/QuartzCore.h>
#import <CoreImage/CoreImage.h>

#define kMapboxID @"dev-test-coverco.i00fmbdk"

static BOOL editMode;

@interface CMLViewController () <RMMapViewDelegate, UIGestureRecognizerDelegate>
{
    BOOL viewLoad;
    BOOL infoClosed;
    UIImage *blurredImage;
}

@property (nonatomic, strong) UIView *blurredContainerView;
@property (nonatomic, strong) UIView *transparentView;

@property (nonatomic, strong) RMMapView *mapView;
@property (nonatomic, strong) RMMapOverlayView *overlayView;
@property (nonatomic, strong) NSMutableArray *vectors;

@property (nonatomic, strong) UIButton *editButton;
@property (nonatomic, strong) UITapGestureRecognizer *tap;

@end

@implementation CMLViewController

-(UIView *)blurredContainerView
{
    if (!_blurredContainerView) _blurredContainerView = [[UIView alloc] initWithFrame:self.view.frame];
    return _blurredContainerView;
}

-(UIView *)transparentView
{
    if (!_transparentView) _transparentView = [[UIView alloc] initWithFrame:self.view.frame];
    return _transparentView;
}

-(NSMutableArray *)vectors
{
    if (!_vectors) _vectors = [[NSMutableArray alloc] init];
    return _vectors;
}

-(UITapGestureRecognizer *)tap
{
    if (!_tap) _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSingleTap:)];
    return _tap;
}

-(UIButton *)editButton
{
    if (!_editButton) _editButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 50, 50)];
    return _editButton;
}

-(RMMapView *)mapView
{
    if (!_mapView) _mapView = [[RMMapView alloc] initWithFrame:self.view.frame];
    return _mapView;
}

-(id)init
{
    if (self = [super init])
    {
        editMode = NO;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.blurredContainerView.backgroundColor = [UIColor clearColor];
    self.blurredContainerView.alpha = 0.0f;
    
    self.transparentView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
    
    [self.blurredContainerView addSubview:self.transparentView];
    [self.view addSubview:self.blurredContainerView];
    
    
    self.mapView.hideAttribution = YES;
    self.mapView.showLogoBug = NO;
    
    viewLoad = YES;
    infoClosed = YES;
    
    self.mapView.tileSource = [[RMMapboxSource alloc] initWithMapID:kMapboxID];
    self.mapView.zoom = 12;
    self.mapView.showsUserLocation = YES;
    
    self.mapView.delegate = self;
    [self.view addSubview:self.mapView];
    
    [self.editButton setTitle:@"+" forState:UIControlStateNormal];
    [self.editButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.editButton.titleLabel setFont:[UIFont fontWithName:@"Avenir" size:40]];
    self.editButton.backgroundColor = [UIColor whiteColor];
    self.editButton.layer.cornerRadius = 25.0f;
    [self.editButton addTarget:self action:@selector(editButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.editButton];
}

- (void) captureBlur
{
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Blur image
    CIImage *imageToBlur = [CIImage imageWithCGImage:viewImage.CGImage];
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [gaussianBlurFilter setValue:imageToBlur forKeyPath:@"inputImage"];
    [gaussianBlurFilter setValue:[NSNumber numberWithFloat:10.0f] forKeyPath:@"inputRadius"];
    CIImage *resultImage = [gaussianBlurFilter valueForKey:@"outputImage"];
    
    // create UIImage from filtered image
    blurredImage = [[UIImage alloc] initWithCIImage:resultImage];
    
    //Place the uiimage in a uiimageview
    UIImageView *newView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    newView.image = blurredImage;
    
    // insert blur
    [self.blurredContainerView insertSubview:newView belowSubview:self.transparentView];
}

- (void)didSingleTap:(id)sender
{
//    self.blurredContainerView.alpha = 1.0f;
//    [self.view bringSubviewToFront:self.blurredContainerView];
    //dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self captureBlur];
//    });
    
    
    
    UITapGestureRecognizer *tap = sender;
    
    CGPoint point = [tap locationInView:self.mapView];
    
    for (int i=0; i<[self.vectors count]; i++)
    {
        int j = i + 1;
        if (j >= [self.vectors count])
            j = 0;
        
        if ([self isOnLine:point loc2:[[CMLVector getArrayOfLocations:self.vectors] objectAtIndex:i] loc3:[[CMLVector getArrayOfLocations:self.vectors] objectAtIndex:j]])
        {
            CMLVector *newVector = [CMLVector vectorWithPoint:point mapView:self.mapView];
            
            UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
            [newVector.pointView addGestureRecognizer:panRecognizer];
            UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
            [newVector.pointView addGestureRecognizer:longPressRecognizer];
            newVector.pointView.backgroundColor = [UIColor purpleColor];
            
            [self.vectors insertObject:newVector atIndex:j];
            
            // Need to start with dragging point index so that the RMShape reshapes with the dragging point
            self.vectors = [self reorderObjectsInArray:self.vectors startingWithIndex:j];
            
            RMAnnotation *annotation = [[RMAnnotation alloc] initWithMapView:self.mapView coordinate:newVector.location.coordinate andTitle:@"POI"];
            annotation.userInfo = self.vectors;
            
            // Just trying to remove the annotation for RMShape
            for (RMAnnotation *ann in self.mapView.annotations) {
                if (!ann.isUserLocationAnnotation)
                {
                    [self.mapView removeAnnotation:ann];
                    break;
                }
            }
            
            [self.mapView addAnnotation:annotation];
            [self.view addSubview:newVector.pointView];
            newVector.pointView.transform = CGAffineTransformMakeScale(0.15f, 0.15f);
            [UIView animateWithDuration:0.4f delay:0.0f usingSpringWithDamping:0.6f initialSpringVelocity:0.0f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
                newVector.pointView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
            } completion:nil];
            break;
        }
    }
}

- (void)editButtonPressed
{
    if (editMode) {
        [UIView animateWithDuration:0.2f animations:^{
            self.editButton.frame = CGRectMake(20.0f, 20.0f, 50.0f, 50.0f);
            [self.editButton setTitle:@"+" forState:UIControlStateNormal];
        }];
        
        editMode = NO;
        [self.mapView removeGestureRecognizer:self.tap];
        for (CMLVector *vector in self.vectors) {
            [vector.pointView removeFromSuperview];
        }
        [self.mapView removeAllAnnotations];
        [self.vectors removeAllObjects];
        
        
    } else {
        [UIView animateWithDuration:0.2f animations:^{
            self.editButton.frame = CGRectMake(self.view.frame.size.width - 20.0f - self.editButton.frame.size.width, 20.0f, 50.0f, 50.0f);
            [self.editButton setTitle:@"x" forState:UIControlStateNormal];
        }];
        editMode = YES;
        
        [self.mapView addGestureRecognizer:self.tap];
        
        NSDictionary *dictionary = [CMLPOIEngine createDefaultTriangleWithMapView:self.mapView view:self.view];
        self.vectors = [CMLVector addVectorObjectsFromLocationArray:[dictionary objectForKey:@"locations"] usingMapView:self.mapView];
        for (CMLVector *vector in self.vectors) {
            UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
            [vector.pointView addGestureRecognizer:panRecognizer];
            
            [self.view addSubview:vector.pointView];
        }
        
        if ([self.vectors count])
        {
            CMLVector *vector = [self.vectors objectAtIndex:0];
            CLLocation *loc = vector.location;
            
            RMAnnotation *annotation = [[RMAnnotation alloc] initWithMapView:self.mapView coordinate:loc.coordinate andTitle:@"POI"];
            
            annotation.userInfo = self.vectors;
            //[annotation setBoundingBoxFromLocations:self.vectors];
            [self.mapView addAnnotation:annotation];
        }
    }
}

- (void)vectorCreationSetup:(CMLVector *)vector
{
    
}

- (void)handlePan: (UIPanGestureRecognizer *) pan
{
    CMLPointView *pointView = (CMLPointView *)pan.view;
    CGPoint translation = [pan translationInView:self.view];
    
    if (pan.state == UIGestureRecognizerStateBegan)
    {
        [UIView animateWithDuration:0.1 animations:^{
            pointView.transform = CGAffineTransformMakeScale(2.5f, 2.5f);
        }];
    }
    else if (pan.state == UIGestureRecognizerStateEnded)
    {
        [UIView animateWithDuration:0.1 animations:^{
            pointView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        }];
    }
    
    pointView.center = CGPointMake(pointView.lastLocation.x + translation.x,
                                   pointView.lastLocation.y + translation.y);
    
    NSUInteger index = 0;
    for (CMLVector *vec in self.vectors)
    {
        if (CGRectContainsPoint(pointView.frame, CGPointMake([self.mapView coordinateToPixel:vec.location.coordinate].x, [self.mapView coordinateToPixel:vec.location.coordinate].y)))
        {
            index = [self.vectors indexOfObject:vec];
            break;
        }
    }
    
    [self.vectors removeObjectAtIndex:index];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:[self.mapView pixelToCoordinate:pointView.center].latitude longitude:[self.mapView pixelToCoordinate:pointView.center].longitude];
    [self.vectors insertObject:[[CMLVector alloc] initWithPointView:pointView andLocation:loc] atIndex:index];
    
    // Need to start with dragging point index so that the RMShape reshapes with the dragging point
    self.vectors = [self reorderObjectsInArray:self.vectors startingWithIndex:index];
    
    // Just trying to remove the annotation for RMShape
    for (RMAnnotation *ann in self.mapView.annotations) {
        if (!ann.isUserLocationAnnotation)
        {
            [self.mapView removeAnnotation:ann];
            break;
        }
    }
    
    RMAnnotation *annotation = [[RMAnnotation alloc] initWithMapView:self.mapView coordinate:[self.mapView pixelToCoordinate:pointView.center] andTitle:@"POI"];
    annotation.userInfo = self.vectors;
    //[annotation setBoundingBoxFromLocations:self.poiCoordinates];
    [self.mapView addAnnotation:annotation];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)longPress
{
    CMLPointView *pointView = (CMLPointView *)longPress.view;
    
    if (longPress.state == UIGestureRecognizerStateEnded)
    {
        [UIView animateWithDuration:0.1f animations:^{
            pointView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        }];
    }
}

- (NSMutableArray *)reorderObjectsInArray:(NSArray *)arrayToOrder startingWithIndex:(NSUInteger)startIndex
{
    NSMutableArray *newOrderArray = [[NSMutableArray alloc] init];
    
    NSUInteger index = startIndex;
    for (int i = 0; i < [arrayToOrder count]; i++) {
        [newOrderArray insertObject:[arrayToOrder objectAtIndex:index] atIndex:i];
        
        index += 1;
        if (index >= [arrayToOrder count])
            index = 0;
    }
    
    return newOrderArray;
}

-(RMMapLayer *)mapView:(RMMapView *)mapView layerForAnnotation:(RMAnnotation *)annotation
{
    if (annotation.isUserLocationAnnotation)
        return nil;
    
    RMShape *shape = [[RMShape alloc] initWithView:mapView];
    shape.lineColor = [UIColor purpleColor];
    shape.fillColor = [[UIColor purpleColor] colorWithAlphaComponent:0.4f];
    shape.lineWidth = 3.0;
    
    NSArray *locations = [CMLVector getArrayOfLocations:(NSArray *)annotation.userInfo];
    CLLocation *start = (CLLocation *)[locations objectAtIndex:0];
    for (CLLocation *loc in locations) {
        [shape addLineToCoordinate:loc.coordinate];
    }
    [shape addLineToCoordinate:start.coordinate];
    return shape;
}

-(void)mapViewRegionDidChange:(RMMapView *)mapView
{
    // update PointViews with CLLocations
    for (CMLVector *vector in self.vectors) {
        CGFloat height = vector.pointView.frame.size.height;
        CGFloat width = vector.pointView.frame.size.width;
        vector.pointView.frame = CGRectMake([mapView coordinateToPixel:vector.location.coordinate].x - (width/2),
                                            [mapView coordinateToPixel:vector.location.coordinate].y - (height/2),
                                            width, height);
    }
}

- (BOOL)isOnLine:(CGPoint)point loc2:(CLLocation *)loc2 loc3:(CLLocation *)loc3
{
    CGPoint point1 = [self.mapView coordinateToPixel:loc2.coordinate];
    CGPoint point2 = [self.mapView coordinateToPixel:loc3.coordinate];
    
    double xdiff = pow((fabs(point1.x - point2.x)), 2);
    double ydiff = pow((fabs(point1.y - point2.y)), 2);
    double distance = sqrt(xdiff + ydiff);
    
    UIView *viewRect = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, distance)];
    
    double xmid = (point1.x + point2.x)/2;
    double ymid = (point1.y + point2.y)/2;
    
    CGRect newRect = viewRect.frame;
    newRect.origin.x = xmid - viewRect.frame.size.width/2;
    newRect.origin.y = ymid - viewRect.frame.size.height/2;
    viewRect.frame = newRect;
    
    CGFloat x = point1.x - point2.x;
    CGFloat y = point1.y - point2.y;
    CGFloat m = (y / x);
    
    [self.view addSubview:viewRect];
    
    if (CGRectContainsPoint(viewRect.frame, [viewRect newPointInView:point andSlope:m]))
    {
        [viewRect removeFromSuperview];
        return YES;
    }
    else
    {
        [viewRect removeFromSuperview];
        return NO;
    }
}

-(void)mapView:(RMMapView *)mapView didUpdateUserLocation:(RMUserLocation *)userLocation
{
    // zoom in to markers after launch
    //
    
    if (viewLoad)
    {
        __weak RMMapView *weakMap = self.mapView; // avoid block-based memory leak
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC), dispatch_get_main_queue(), ^(void)
                       {
                           float degreeRadius = 9000.f / 110000.f; // (9000m / 110km per degree latitude)
                           
                           //CLLocationCoordinate2D centerCoordinate = [((RMMapboxSource *)self.mapView.tileSource) centerCoordinate];
                           CLLocationCoordinate2D centerCoordinate = userLocation.coordinate;
                           
                           RMSphericalTrapezium zoomBounds = {
                               .southWest = {
                                   .latitude  = centerCoordinate.latitude  - degreeRadius,
                                   .longitude = centerCoordinate.longitude - degreeRadius
                               },
                               .northEast = {
                                   .latitude  = centerCoordinate.latitude  + degreeRadius,
                                   .longitude = centerCoordinate.longitude + degreeRadius
                               }
                           };
                           
                           [weakMap zoomWithLatitudeLongitudeBoundsSouthWest:zoomBounds.southWest
                                                                   northEast:zoomBounds.northEast
                                                                    animated:YES];
                       });
        viewLoad = NO;
    }
}

@end
