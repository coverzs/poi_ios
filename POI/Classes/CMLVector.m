//
//  CMLVector.m
//  POI
//
//  Created by Zach Stegall on 8/6/14.
//  Copyright (c) 2014 Cover Media Labs. All rights reserved.
//

#import "CMLVector.h"

static CGFloat pointViewSize = 20.0f;

@implementation CMLVector

-(instancetype)init
{
    if (self = [super init])
    {
        
    }
    
    return self;
}

-(instancetype)initWithPointView:(CMLPointView *)view andLocation:(CLLocation *)loc
{
    if (self = [super init])
    {
        self.pointView = view;
        self.location = loc;
    }
    
    return self;
}

+ (NSMutableArray *)addVectorObjectsFromLocationArray:(NSArray *)array usingMapView:(RMMapView *)mapView
{
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    
    for (CLLocation *loc in array) {
        if (![loc isKindOfClass:[CLLocation class]])
            return nil;
        else
        {
            CMLVector *vector = [[CMLVector alloc] initWithPointView:[[CMLPointView alloc] initWithFrame:CGRectMake([mapView coordinateToPixel:loc.coordinate].x - pointViewSize/2,
                                                                                                                    [mapView coordinateToPixel:loc.coordinate].y - pointViewSize/2,
                                                                                                                    pointViewSize, pointViewSize)]
                                                         andLocation:loc];
            vector.pointView.backgroundColor = [UIColor purpleColor];
            [newArray addObject:vector];
        }
    }
    
    return newArray;
}

+ (CMLVector *)vectorWithLocation:(CLLocation *)loc mapView:(RMMapView *)mapView
{
    CMLVector *vector = [[CMLVector alloc] init];
    
    vector.location = loc;
    vector.pointView = [[CMLPointView alloc] initWithFrame:CGRectMake([mapView coordinateToPixel:loc.coordinate].x, [mapView coordinateToPixel:loc.coordinate].y, pointViewSize, pointViewSize)];
    
    return vector;
}

+ (CMLVector *)vectorWithPoint:(CGPoint)point mapView:(RMMapView *)mapView
{
    CMLVector *vector = [[CMLVector alloc] init];
    
    vector.location = [[CLLocation alloc] initWithLatitude:[mapView pixelToCoordinate:point].latitude longitude:[mapView pixelToCoordinate:point].longitude];
    vector.pointView = [[CMLPointView alloc] initWithFrame:CGRectMake(point.x-(pointViewSize/2), point.y-(pointViewSize/2), pointViewSize, pointViewSize)];
    
    return vector;
}

+ (NSArray *)getArrayOfLocations:(NSArray *)array
{
    NSMutableArray *locations = [[NSMutableArray alloc] init];
    for (CMLVector *vector in array) {
        [locations addObject:vector.location];
    }
    
    return locations;
}

+ (NSArray *)getArrayOfPointViews:(NSArray *)array
{
    NSMutableArray *pointViews = [[NSMutableArray alloc] init];
    for (CMLVector *vector in array) {
        [pointViews addObject:vector.pointView];
    }
    
    return pointViews;
}

@end
