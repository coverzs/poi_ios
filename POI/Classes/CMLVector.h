//
//  CMLVector.h
//  POI
//
//  Created by Zach Stegall on 8/6/14.
//  Copyright (c) 2014 Cover Media Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <Mapbox/Mapbox.h>
#import "CMLPointView.h"

@interface CMLVector : NSObject
@property (strong, nonatomic) CMLPointView *pointView;
@property (strong, nonatomic) CLLocation *location;

- (instancetype)init;
- (instancetype)initWithPointView:(CMLPointView *)view andLocation:(CLLocation *)loc;

+ (NSMutableArray *)addVectorObjectsFromLocationArray:(NSArray *)array usingMapView:(RMMapView *)mapView;

+ (NSArray *)getArrayOfLocations:(NSArray *)array;
+ (NSArray *)getArrayOfPointViews:(NSArray *)array;

+ (CMLVector *)vectorWithLocation:(CLLocation *)loc mapView:(RMMapView *)mapView;
+ (CMLVector *)vectorWithPoint:(CGPoint)point mapView:(RMMapView *)mapView;
@end
