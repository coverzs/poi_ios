//
//  CMLPOIEngine.m
//  POI
//
//  Created by Zach Stegall on 8/4/14.
//  Copyright (c) 2014 Cover Media Labs. All rights reserved.
//

#import "CMLPOIEngine.h"

#define degreesToRadians( degrees ) ( ( degrees ) / 180.0 * M_PI )
#define radiansToDegrees( radians ) ( ( radians ) * ( 180.0 / M_PI ) )

@implementation CMLPOIEngine

+ (NSDictionary *)createDefaultTriangleWithMapView:(RMMapView *)mapView view:(UIView *)view
{
    CGPoint topLeft = CGPointMake(view.frame.size.width * 0.25f, view.frame.size.height * 0.25f);
    CGPoint topRight = CGPointMake(view.frame.size.width * 0.75f, view.frame.size.height * 0.25f);
    CGPoint bottomCenter = CGPointMake(view.frame.size.width * 0.5f, view.frame.size.height * 0.75f);
    
    RMAnnotation *annotationTopLeft = [[RMAnnotation alloc] initWithMapView:mapView coordinate:[mapView pixelToCoordinate:topLeft] andTitle:@"topleft"];
    annotationTopLeft.userInfo = @"Circle";
    RMAnnotation *annotationTopRight = [[RMAnnotation alloc] initWithMapView:mapView coordinate:[mapView pixelToCoordinate:topRight] andTitle:@"topright"];
    annotationTopRight.userInfo = @"Circle";
    RMAnnotation *annotationBottomCenter = [[RMAnnotation alloc] initWithMapView:mapView coordinate:[mapView pixelToCoordinate:bottomCenter] andTitle:@"bottomcenter"];
    annotationBottomCenter.userInfo = @"Circle";
    
    NSArray *arrayOfLocations = [NSArray arrayWithObjects:[[CLLocation alloc] initWithLatitude:annotationTopLeft.coordinate.latitude longitude:annotationTopLeft.coordinate.longitude],
                                 [[CLLocation alloc] initWithLatitude:annotationTopRight.coordinate.latitude longitude:annotationTopRight.coordinate.longitude],
                                 [[CLLocation alloc] initWithLatitude:annotationBottomCenter.coordinate.latitude longitude:annotationBottomCenter.coordinate.longitude], nil];
    
    NSArray *arrayOfAnnotations = [NSArray arrayWithObjects:annotationTopLeft, annotationTopRight, annotationBottomCenter, nil];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:@[arrayOfLocations, arrayOfAnnotations] forKeys:@[@"locations", @"annotations"]];
    
    return dictionary;
}

// arrayOfCoordinates come in objects of CLLocation
+ (NSArray *)orderPoints:(NSArray *)arrayOfCoordinates
{
    NSArray *orderedArray = [[NSArray alloc] init];
    
    CLLocation *center = [self findCenter:arrayOfCoordinates];
    
    if (arrayOfCoordinates)
    {
        for (CLLocation *loc in arrayOfCoordinates)
        {
            CGFloat angle = atan2f( degreesToRadians(loc.coordinate.latitude - center.coordinate.latitude ),
                                    degreesToRadians(loc.coordinate.longitude - center.coordinate.longitude) );
            NSLog(@"%f ", radiansToDegrees(angle));
        }
    }
    else
    {
        return nil;
    }
    
    return orderedArray;
}

+ (CLLocation *)findCenter:(NSArray *)arrayOfCoordinates
{
    CLLocation *startLoc = (CLLocation *)[arrayOfCoordinates objectAtIndex:0];
    CGFloat largestLatitude = startLoc.coordinate.latitude;
    CGFloat smallestLatitude = startLoc.coordinate.latitude;
    CGFloat largestLongitude = startLoc.coordinate.longitude;
    CGFloat smallestLongitude = startLoc.coordinate.longitude;
    
    for (CLLocation *loc in arrayOfCoordinates) {
        if (loc.coordinate.latitude > largestLatitude)
            largestLatitude = loc.coordinate.latitude;
        
        if (loc.coordinate.latitude < smallestLatitude)
            smallestLatitude = loc.coordinate.latitude;
        
        if (loc.coordinate.longitude > largestLongitude)
            largestLongitude = loc.coordinate.longitude;
        
        if (loc.coordinate.longitude < smallestLongitude)
            smallestLongitude = loc.coordinate.longitude;
    }
    
    CGFloat centerLatitude = ((largestLatitude - smallestLatitude) / 2) + smallestLatitude;
    CGFloat centerLongitude = ((largestLongitude - smallestLongitude) / 2) + smallestLongitude;
    
    CLLocation *center = [[CLLocation alloc] initWithLatitude:centerLatitude longitude:centerLongitude];
    return center;
}

@end
