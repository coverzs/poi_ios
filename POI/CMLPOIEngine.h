//
//  CMLPOIEngine.h
//  POI
//
//  Created by Zach Stegall on 8/4/14.
//  Copyright (c) 2014 Cover Media Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <Mapbox/Mapbox.h>

@interface CMLPOIEngine : NSObject

+ (NSDictionary *)createDefaultTriangleWithMapView:(RMMapView *)mapView view:(UIView *)view;

+ (NSArray *)orderPoints:(NSArray *)arrayOfCoordinates;
+ (CLLocation *)findCenter:(NSArray *)arrayOfCoordinates;

@end
