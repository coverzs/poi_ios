//
//  CMLPointView.h
//  POI
//
//  Created by Zach Stegall on 8/6/14.
//  Copyright (c) 2014 Cover Media Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMLPointView : UIView
@property CGPoint lastLocation;
@end
