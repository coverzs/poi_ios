//
//  CMLPointView.m
//  POI
//
//  Created by Zach Stegall on 8/6/14.
//  Copyright (c) 2014 Cover Media Labs. All rights reserved.
//

#import "CMLPointView.h"

@implementation CMLPointView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.layer.cornerRadius = frame.size.width/2;
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [self addGestureRecognizer:tapRecognizer];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.lastLocation = self.center;
    [UIView animateWithDuration:0.1f animations:^{
        self.transform = CGAffineTransformMakeScale(2.5f, 2.5f);
    }];
}

- (void)handleTap:(UITapGestureRecognizer *)tap
{
    [UIView animateWithDuration:0.1 animations:^{
        self.transform = CGAffineTransformMakeScale(2.5f, 2.5f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            self.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        }];
    }];
}

@end
