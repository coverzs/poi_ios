//
//  CMLAppDelegate.h
//  POI
//
//  Created by Zach Stegall on 8/1/14.
//  Copyright (c) 2014 Cover Media Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
