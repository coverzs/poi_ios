//
//  UIView+Transform.m
//  POI
//
//  Created by Zach Stegall on 8/6/14.
//  Copyright (c) 2014 Cover Media Labs. All rights reserved.
//

#import "UIView+Transform.h"
#define degreesToRadians( degrees ) ( ( degrees ) / 180.0 * M_PI )
#define radiansToDegrees( radians ) ( ( radians ) * ( 180.0 / M_PI ) )

@implementation UIView (Transform)

// helper to get point offset from center
-(CGPoint)centerOffset:(CGPoint)thePoint {
    return CGPointMake(thePoint.x - self.center.x, thePoint.y - self.center.y);
}
// helper to get point back relative to center
-(CGPoint)pointRelativeToCenter:(CGPoint)thePoint {
    return CGPointMake(thePoint.x + self.center.x, thePoint.y + self.center.y);
}
// helper to get point relative to transformed coords
-(CGPoint)newPointInView:(CGPoint)thePoint andSlope:(CGFloat)m {
    // get offset from center
    CGPoint offset = [self centerOffset:thePoint];
    
    // get transformed point
    CGPoint transformedPoint = CGPointApplyAffineTransform(offset, CGAffineTransformMakeRotation((degreesToRadians(90) - atanf(m))));
    
    // make relative to center
    return [self pointRelativeToCenter:transformedPoint];
}

@end
